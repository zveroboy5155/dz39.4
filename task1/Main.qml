import QtQuick
import QtQuick.Window
import QtQuick.Controls 2.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Task1")

    Rectangle{
        id:scene
        anchors.fill: parent
        state: "LeftSt"

        Rectangle{
            id:leftReg
            x: 100
            y: 200
            color: "lightgrey"
            width: 100
            height: 100
            border.color: "black"
            border.width: 4
            radius: 5
            Text {
                //id: name
                anchors.centerIn: parent
                text: "Move"
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    scene.state = "moveSt"
                    if ((ball.x + ball.width/2) >= (rightReg.x - rightReg.width/2)) scene.state = "LeftSt"
                    else ball.x += 10
                }
            }
        }
        Rectangle{
            id: rightReg
            x: 300
            y: 200
            color: "lightgrey"
            width: 100
            height: 100
            border.color: "black"
            border.width: 4
            radius: 5
            Text {
                //id: name
                anchors.centerIn: parent
                text: "Return"
            }

            MouseArea{
                anchors.fill: parent
                onClicked: scene.state = "LeftSt"
            }
        }
        Rectangle{
            id :ball
            color: "purple"
            x: leftReg.x + 5
            y: leftReg.y + 5
            width: leftReg.width - 10
            height:  leftReg.height - 10
            radius: width / 2
        }

        states: [
            State {
                name: "LeftSt"
                PropertyChanges {
                    target: ball
                    x: leftReg.x + 5
                }
            },
            State {
                name: "moveSt"
                PropertyChanges {
                    target: ball
                    x: ball.x
                }
            }
        ]
        transitions: [
            Transition {
                from: "moveSt"
                to: "LeftSt"

                NumberAnimation {
                    properties: "x,y"
                    duration: 1000
                    easing.type: Easing.OutBounce
                }
            }
        ]
    }
}
