
#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QRegularExpression>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->lineEdit, &QLineEdit::textEdited, [this](QString curTxt)
    {
        QRegularExpression exp("^\\+\\d{11}$");
                if(exp.match(curTxt).hasMatch())
            setOkLabel();
        else
            setFailLabel();
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setOkLabel()
{
    ui->label->setText("Number Correct");
    ui->label->setStyleSheet("QLabel {color : green;}");
}

void MainWindow::setFailLabel()
{
    ui->label->setText("Number incorrect");
    ui->label->setStyleSheet("QLabel {color : red;}");
}

//#include "main.moc"
